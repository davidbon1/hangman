#Hangman
#Maxim Strzebkowski & David Bonello
#04.01.18

#Test Commit!!!

import os
import time
import random
import platform

hangman = ["\n\n\n\n\n\n","\n\n\n\n\n\n------",
        "\n\n\n|\n|\n|\n------",
        "\n ___\n/\n|\n|\n|\n------",
        "\n ___\n/   o\n|\n|\n|\n------",
        "\n ___\n/   o\n|   |\n|\n|\n------",
        "\n ___\n/   o\n|   |\n|  /  \n|\n------",
        "\n ___\n/   o\n|   |\n|  /\ \n|\n------",
        "\n ___\n/   o\n|  /|  \n|  /\ \n|\n------",
        "\n ___\n/   o\n|  /|\ \n|  /\ \n|\n------"]

def checkSystem():
	if platform.system() == "Windows":
		return 'cls'
	else:
		return 'clear'

def importWords(fileName):
	with open(fileName, 'r') as file:
		return file.read().splitlines()

def randomWord(wordLibrary):
	return str.lower(random.choice(wordLibrary))

def chooseWord(wordLibrary):
        word = str.lower(input("Think of a word: "))
        if not word in wordLibrary:	
        	wordLibrary.append(word)
        return word

def writeWord(guessed,word):
    for char in word:
    	if char in guessed:
    		print(char,end=" ")
    	else:
    		print("_",end=" ")
    print("\n")

def writeGuessed(guessed):
	if len(guessed) > 0:	
		print("Already Tried:")
		for char in guessed:
        		print(char, end=" ")
		print(" ")
		print(" ")

def checkCorrect(UserChar,word):
	if UserChar in word:
		return True
	else:
		return False

def checkWin(guessed, word):
	for char in word:
		if not char in guessed:
			return False
	return True

def guess(word,guessed,won,tries):
	userInput = str.lower(str(input("Your Guess: ")))

	while userInput in guessed:
		print("You already guessed that!")
		userInput = str(input("Your Guess: "))

	if len(str(userInput)) == 1:
		guessed.append(str(userInput))
		if not checkCorrect(str(userInput),word):
			tries -= 1
		won = checkWin(guessed,word)
	elif len(str(userInput)) > 1:
		if userInput == word:
			won = True
		else:
			print("Incorrect")
			tries -= 1
			time.sleep(1)
	else:
		print("You need to enter something")
		time.sleep(1)
	
	return won, tries

def game(clearCmd,word):
	global hangman

	guessed = []
	won = False
	tries = len(hangman)

	while tries > 0 and won == False:
		os.system(clearCmd)
		print(hangman[len(hangman)-tries])
		print('')
		writeWord(guessed,word)
		writeGuessed(guessed)
		print(str(tries)+" tries left...")
		print(" ")
		won, tries = guess(word,guessed,won,tries)
	if won == True:
		print("Congratulations")
	elif tries <= 0:
		if checkWin(guessed, word):
			print("Congratulations")
		else:
			print("You lose")
			
	print("the word was: " + word)
	input("press ENTER to continue...")

def menu():

	clearCmd = checkSystem()
	wordLibrary = importWords('words.txt')
	
	os.system(clearCmd)
	print("[1] Multiplayer")
	print("[2] Singleplayer")
	print("[3] Exit")
	
	UserInput = input("Choice: ")

	if UserInput == "1":
		game(clearCmd,chooseWord(wordLibrary))
	elif UserInput == "2":
		game(clearCmd,randomWord(wordLibrary))
	elif UserInput == "3":
		return False
	else:
		print("Not a valid Option")
		time.sleep(1)
	return True

def run():
	playing = True
	while playing:
		playing = menu()

run()
